import 'package:dio/dio.dart';
import 'package:shopping_tooth_app/config/config_api.dart';
import 'package:shopping_tooth_app/model/goods_detail_result.dart';
import 'package:shopping_tooth_app/model/goods_list_result.dart';

class RepoGoods {
  /**
   * 복수 R
   */
  Future<GoodsListResult> getGoodss() async { // 나중에 올꺼야<타입>, Goods호출해서 데이터 받는다
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/goods/all'; // 엔드 포인트

    final response = await dio.get(
        _baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) { // 연화 연결 상태, 실패 시는 안 넘겨준다
            return status == 200;
          }
      )
    );

    return GoodsListResult.fromJson(response.data);
  }

  Future<GoodsDetailResult> getGoods(num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/goods/detail/{id}';

    final response = await dio.get(
        _baseUrl.replaceAll('{id}', id.toString()), //{id}를 id스트링으로 바꿔라, replaceAll = 찾아서 같은걸 전부 바꾸기
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return GoodsDetailResult.fromJson(response.data);
  }
}
// 타입을 안쓰면 다이나믹이다
// final 들어올 때까지 기다 렸다가 상수 처리
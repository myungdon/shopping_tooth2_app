import 'package:flutter/material.dart';
import 'package:shopping_tooth_app/components/component_goods_item.dart';
import 'package:shopping_tooth_app/pages/page_goods_deatil.dart';
import 'package:shopping_tooth_app/model/goods_item.dart';
import 'package:shopping_tooth_app/repository/repo_goods.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();
}

class _PageGoodsListState extends State<PageGoodsList> {
  List<GoodsItem> _list = [];

  Future<void> _loadList() async {
    // 나중에 오는데 넌 신경 쓰지마 내가 알아서 할게
    // 이 메서드가 repo 호출해서 데이터 받고
    // setState해서 _list 교체 할거다...
    await RepoGoods().getGoodss()
        .then((res) => {
          setState(() {
            _list = res.list;
          })
        })
        .catchError((err) => {
          debugPrint(err)
        });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('칫솔 리스트'),
      ),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int idx) {
          return ComponentGoodsItem(goodsItem: _list[idx], callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(id: _list[idx].id)));
          });
        },
      ),
    );
  }
}

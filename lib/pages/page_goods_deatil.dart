import 'package:flutter/material.dart';
import 'package:shopping_tooth_app/model/goods_response.dart';
import 'package:shopping_tooth_app/repository/repo_goods.dart';

class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageGoodsDetail> createState() => _GoodsDetailState();
}

class _GoodsDetailState extends State<PageGoodsDetail> {
  GoodsResponse? _detail; // 타입뒤에? = 있을수도 있고 없을수도, 없으면 null

  Future<void> _loadDetail() async { //widget.id로 받을거라서 ()에 안적어도 된다
    await RepoGoods().getGoods(widget.id)
        .then((res) => {
          setState(() {
          _detail = res.data;
      })
    });
  }

  @override
  void initState(){
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("상세 페이지"),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context){
    if (_detail == null){ // 스켈레톤 ui때문에 if문 처리함 = 로딩중에 로딩화면 보여주는 것
      return Text('데이터 로딩중'); // 데이터가 없으면 로딩중 처리
    } else{ // 데이터가 있으면 보여주겠다
      return ListView(
        children: [
          Text('${widget.id}'),
          Text(_detail!.thumbImg),  // ! = 무조건 있을꺼야 (!처리 안하면 에러가 남)
          Text(_detail!.name),
          Text('${_detail!.price}원'),
          Text(_detail!.dateCreate),
        ],
      );
    }
  }
}

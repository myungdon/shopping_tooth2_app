import 'package:shopping_tooth_app/model/goods_item.dart';

class GoodsResponse {
  num id;
  String thumbImg;
  String name;
  num price;
  String dateCreate;

  GoodsResponse(this.id, this.thumbImg, this.name, this.price, this.dateCreate);

    factory GoodsResponse.fromJson(Map<String, dynamic> json){
    return GoodsResponse(
        json['id'],
        json['thumbImg'],
        json['name'],
        json['price'],
        json['dateCreate'],
    );
  }
}
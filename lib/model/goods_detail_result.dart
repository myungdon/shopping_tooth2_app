import 'package:shopping_tooth_app/model/goods_response.dart';

class GoodsDetailResult {
  String mgs;
  num code;
  GoodsResponse data;

  GoodsDetailResult(this.mgs, this.code, this.data);

  factory GoodsDetailResult.fromJson(Map<String, dynamic> json){
    return GoodsDetailResult(
        json['msg'],
        json['code'],
        GoodsResponse.fromJson(json['data']) // 맨위의 data로 바꿔줘
    );
  }
}
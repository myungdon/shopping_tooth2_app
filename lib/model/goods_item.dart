class GoodsItem {
  num id;
  String thumbImg;
  String name;
  num price;

  GoodsItem(this.id, this.thumbImg, this.name, this.price);

  factory GoodsItem.fromJson(Map<String, dynamic> json) { // . 기준 앞이 리턴타입, . 기준 뒤에꺼는 메서드 이름 / json을 받아서 굿즈아이템으로 바꿔줌
    return GoodsItem(
      json['id'],
      json['thumbImg'],
      json['name'],
      json['price'],
    );
  }
}
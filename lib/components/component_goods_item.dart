import 'package:flutter/material.dart';
import 'package:shopping_tooth_app/model/goods_item.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem({
    super.key,
    required this.goodsItem,
    required this.callback,
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Text(goodsItem.thumbImg),
            Text(goodsItem.name),
            Text(('${goodsItem.price}원'))
          ],
        ),
      ),
    );
  }
}
